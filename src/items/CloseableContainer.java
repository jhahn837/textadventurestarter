package items;

import interfaces.Closeable;
import textadventure.World;

public class CloseableContainer extends Container implements Closeable {

	private boolean isOpen;
	
	public CloseableContainer(World world, String name, int weight, boolean takeable, String description, boolean open) {
		super(world, name, weight, takeable, description);
		isOpen = open;
	}

	@Override
	public boolean isOpen() {
		return isOpen;
	}

	@Override
	public void doOpen() {
		if(isOpen) {
			World.print("Done.\n\n");
		}else {
			World.print("Opened.\n\n");
		}
	}

	@Override
	public void doClose() {
		if(isOpen) {
			World.print("Closed.\n\n");
		}else {
			World.print("Done.\n\n");
		}
	}

	@Override
	public void doExamine() {
		if(isOpen) {
			World.print("Inside the " + super.getName() + " you see " + super.getItems() + ".\n\n");
		}else {
			World.print("The " + super.getName() + " is closed.\n\n");
		}
	}
	
	@Override
	public Item doTake(Item item) {
		if(isOpen) {
			return super.doTake(item);
		}else {
			World.print("The " + super.getName() + " is closed.\n\n");
			return null;
		}
	}
}
