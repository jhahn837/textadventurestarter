package items;

import textadventure.World;
import textadventure.ZooBreakout;

public class Key extends Item {

	public Key(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doUse() {
		if(getWorld().getPlayer().getCurrentRoom().getName().equals("THE ZOO PLAZA")) {
			World.print("Ooh! You have unlocked the gate to the Ferocious Felines Safari!\n\n"
						+ "But watch out! It just happens so that a black panther had escaped and is "
						+ "now looking for dinner...\n\nWhat will you do?\n\n");
			World.print("Options:\n"
						+ "(1) Run for your life\n"
						+ "(2) Hide\n"
						+ "(2) Freeze and try to make it ignore you\n\n");
		}
	}

}
