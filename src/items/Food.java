package items;

import interfaces.Edible;
import textadventure.World;

public class Food extends Item implements Edible {

	public Food(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doEat() {
		if(this.getWeight() == 0) {
			if(this.getName().equals("bucket_of_popcorn")) {
				World.print("The popcorn looks stale and a little moldy, but you eat it anyway...\n");
			}
			World.print("You eat the " + super.getName() + " and start to feel sick...\n");
			World.print("Before you know it, your vision starts to fade and you collapse!\n" +
						"You end up missing your flight and giving up on the competition.\n\nGame Over!\n\n");
			getWorld().setGameOver(true);
		}else {
			World.print("You eat the " + super.getName() + " and feel stronger!\n\n");
			getWorld().getPlayer().removeItem(this);
			getWorld().getPlayer().setHealth(getWorld().getPlayer().getHealth() + this.getWeight());
		}
	}

	@Override
	public void doUse() {
		doEat();
	}

}
