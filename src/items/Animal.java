package items;

import textadventure.World;

public class Animal extends ItemLockedContainer {

	public Animal(World world, String name, int weight, boolean takeable, String description, boolean open,
			boolean locked, String key) {
		super(world, name, weight, takeable, description, open, locked, key);
	}

	@Override
	public Item doGive(Item item, Container source) {
		addItem(item);
		source.removeItem(item);	
		
		if(this.getName().equals("giraffe")) {
			World.print("The giraffe draws near to you and lowers itself to take the carrot from you.\n\n");
			World.print("But oh? What is this?\n");
			getWorld().getPlayer().getCurrentRoom().addItem(new UselessItem(getWorld(), "rare offer to get on it", 0, Item.NOT_TAKEABLE, ""));
			return new UselessItem(getWorld(), "rare offer to get on it", 0, Item.NOT_TAKEABLE, "");
		}
		if(this.getName().equals("crimson_parrot")) {
			this.setDescription("After giving you the key, the crimson_parrot looks smug. For some reason, it keeps shrieking 'Key. Safari. Panther!'...\n");
			return new Key(getWorld(), "key", 0, Item.TAKEABLE, "A rusted key that looks as if it is supposed to unlock something.");
		}
		if(this.getName().equals("emerald_parrot")) {
			this.setDescription("After feeding it some seeds, the emerald_parrot looks content. For some reason, it keeps squawking 'Food. Giraffe. Tree.'...\n");
			return new UselessItem(getWorld(), "whistle of contentment", 0, Item.NOT_TAKEABLE, "A sign that the parrot is happy.");
		}
		if(this.getName().equals("deer")) {
			return new UselessItem(getWorld(), "quarter", 0, Item.TAKEABLE, "A sparkling quarter that glints back at you.");
		}
		return item;
	}
	
	@Override
	public void doOpen() {
		World.print("You can't forcibly open animals! That's cruel...\n\n");
	}
	
	@Override
	public void doExamine() {
		World.print(getDescription() + "\n\n");		
	}
}
