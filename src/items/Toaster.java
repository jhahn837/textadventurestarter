package items;

import textadventure.World;

public class Toaster extends Container {

	public Toaster(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doUse() {
		if (numItems() > 1) {
			World.print("There are too many items in the [container]");
			return;
		}	
		if (numItems() <= 0) {
			World.print("There's nothing in the [container] to toast");
			return;
		}
		if (getItems().get(0).getName().equals("bread")) {
			World.print("You toast the " + getItems().get(0) + ", transforming it into...");
			removeItem("bread");
			addItem(new Food(getWorld(), "toast", 2, Item.TAKEABLE, "A perfectly crisp yet soft slice of toast."));
		}else {
			World.print("You attempt to toast the [item] and burn the house down!  Game over :(");
			System.exit(0);	
		}	

	}
}
