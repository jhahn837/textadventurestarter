package items;

import textadventure.World;

public class Feeder extends ItemLockedContainer {

	public Feeder(World world, String name, int weight, boolean takeable, String description, boolean open,
			boolean locked, String key) {
		super(world, name, weight, takeable, description, open, locked, key);
	}

	@Override
	public Item doGive(Item item, Container source) {
		addItem(item);
		source.removeItem(item);	
		
		if(this.getName().equals("giraffe_feeder")) {
			return new Food(getWorld(), "carrot", 0, Item.TAKEABLE, "Some carrots, the kind that giraffes love to eat.");
		}
		if(this.getName().equals("bird_feeder")) {
			return new Food(getWorld(), "walnut", 0, Item.TAKEABLE, "Some seeds, the kind that birds love to eat.");
		}
		if(this.getName().equals("deer_feeder")) {
			return new Food(getWorld(), "chesnut", 0, Item.TAKEABLE, "Some pellets, a treat for the deer at the zoo.");
		}
		
		return item;
	}	
	
	@Override
	public void doExamine() {
		World.print(getDescription() + "\n\n");		
	}
}
