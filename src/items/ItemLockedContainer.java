package items;

import textadventure.World;
public class ItemLockedContainer extends CloseableContainer{
		
	/** the item needed to unlock the container*/
	private String keyItem;
	
	private boolean isLocked;
	
	public static final boolean LOCKED = true;
	public static final boolean UNLOCKED = false;
	
	public ItemLockedContainer(World world, String name, int weight, boolean takeable, String description,
			boolean open, boolean locked, String key) {
		super(world, name, weight, takeable, description, open);
		isLocked = locked;
		keyItem = key;
	}

	public String getKeyItem() {
		return keyItem;
	}
	
	@Override
	public void doOpen() {
		if(isOpen()) {
			World.print("Done.\n\n");
		}else if(!isOpen() && isLocked){
			World.print("The " + super.getName() + " is locked.\n\n");
		}else if(!isOpen() && !isLocked) {
			World.print("Opened.\n\n");
		}
	}
	
	//put item into itemcontainer, take newitem from source
	public Item doGive(Item item, Container source) {
		addItem(item);
		source.removeItem(item);		
		return item;
	}

}
