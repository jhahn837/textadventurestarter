package command;

import textadventure.World;

public class CommandHints extends Command{

	@Override
	public String[] getCommandWords() {
		return new String[]{"hints"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if (params.length == 1 && params[0].equals("please")) {
			World.print("Special Hints and Tips:\n");
			World.print("- Giving feeders the right coins lets you feed animals!\n");
			World.print("- Animals are easily swayed by food\n");
			World.print("- Red is bad and green is good\n");
			World.print("- Giraffes are so tall, their necks can even reach the trees surrounding the zoo\n");
			World.print("- Parrots are birds that can speak or mimic sounds\n");
			World.print("- If only you could get to a tree outside the zoo and climb down it...\n");
			World.print("\n");		
		}else if(params.length > 0) {
			Command handler = Command.getCommandHandlerForWord(params[0]);
			if (handler == null || handler.getHelpDescription() == null) {
				World.print("I have nothing to say about " + params[0] + "\n\n");
			} else {
				World.print(Command.getHelpDescription(handler) + "\n\n");
			}
		} else {
			World.print("General Hints and Tips:\n");
			World.print("- Examine all objects that can be examined to understand the story better\n");
			World.print("- Different actions could bring different changes, so try re-examining objects too\n");
			World.print("- One wrong move could end the game so be careful!\n");
			World.print("- Note that not all animals might be trying to help you...\n");
			World.print("- Make sure you know ALL the commands that are available to you\n");
			World.print("- Try asking for hints nicely for special hints and tips\n");
			World.print("\n");			
		}
	}

	@Override
	public String getHelpDescription() {
		return "";
	}

}
