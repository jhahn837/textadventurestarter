package command;

import items.Animal;
import items.Container;
import items.Feeder;
import items.Food;
import items.Item;
import items.ItemLockedContainer;
import textadventure.World;

public class CommandGive extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[] {"give", "offer"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		//be || instead?
		if (params.length != 3 || !params[1].equals("to")) {
			World.print("Invalid syntax\n\n");
			return;
		}
		if (!world.getPlayer().getCurrentRoom().hasItem(params[0]) && !world.getPlayer().hasItem(params[0])){
			World.print("You can't see any " + params[0] + " here!\n\n");
			return;
		}
		if (!world.getPlayer().getCurrentRoom().hasItem(params[2]) && !world.getPlayer().hasItem(params[0])){
			World.print("You can't see any " + params[2] + " here!\n\n");
			return;
		}
		if (!(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof ItemLockedContainer)) {
			World.print("The " + params[2] + " can't accept things.\n\n");
			return;
		}
		if (params[0].equals(params[2])) {
			World.print("You can't give something to itself!\n\n");
			return;
		}
		// If we made it this far, then it's safe to give [item]
		// to [container]

		if(((ItemLockedContainer)world.getPlayer().getCurrentRoom().getItem(params[2])).getKeyItem().equals(params[0])) {
			if (world.getPlayer().hasItem(params[0])) {
				if(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof Animal) {
					Item returnItem = ((Animal)world.getPlayer().getCurrentRoom().getItem(params[2])).doGive(world.getPlayer().getItem(params[0]), world.getPlayer());
					world.getPlayer().getCurrentRoom().addItem(returnItem);
					World.print("The " + params[2] + " gave you a " + returnItem.getName() + " in return!\n\n");
					if(params[2].equals("giraffe")) {
						World.print("What will you choose to do?\n\n");
					}
				}else if(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof Feeder){
					Item returnItem = ((Feeder)world.getPlayer().getCurrentRoom().getItem(params[2])).doGive(world.getPlayer().getItem(params[0]), world.getPlayer());
					world.getPlayer().getCurrentRoom().addItem(returnItem);
					World.print("The " + params[2] + " gave you a " + returnItem.getName() + " in return!\n\n");
				}
			}else { // current room has the item
				World.print("You can't give away something you don't possess!\n\n");
				return;
			}	
		}else {
			if(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof Animal) {
				World.print("Hm... it appears the " + params[2] + " doesn't want that item.\n\n");
			}else {
				World.print("Hm... it appears the " + params[2] + " doesn't accept that item.\n\n");
			}
			return;
		}
	}

	//make more clear?
	@Override
	public String getHelpDescription() {
		return "[item] to [itemLockedContainer]";
	}

}
