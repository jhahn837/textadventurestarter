package command;

import textadventure.World;

public class CommandChoose extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[] {"choose", "option"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if(params.length > 1) {
			World.print("Invalid syntax\n\n");
			return;
		}
		if(params.length <= 0) {
			World.print("Which option would you like to choose?\n\n");
			return;
		}
		if(params[0].equals("1")) {
			World.print("Hair flying and mind blanking from fright, you run faster than "
						+ "you ever thought you could. However, with your breathing getting "
						+ "more ragged by the second, you tire out and trip over the keys"
						+ "you had dropped when you open the gate. Unluckily for you, the "
						+ "panther seizes the golden opportunity to eat dinner...\n"
						+ "You get mauled and unfortunately pass away that night...\n\n"
						+ "Game Over!\n\n");
			world.setGameOver(true);
		}else if(params[0].equals("2")) {
			World.print("Faster than humanly possible, you run behind a row of large trash cans "
						+ "nearby. The smell is almost as deadly as the panther, but you endure. "
						+ "Thanks to the pungent smell of garbage, the panther does not notice you "
						+ "and stalks off. You quickly run back out to the plaza, and lock the gate "
						+ "to the Ferocious Feline Exhibit. You sigh out of relief as you were spared\n"
						+ "from becoming a panther's meal.\n"
						+ "Carry on!\n\n");
		}else {
			World.print("You freeze and don't even breathe as you wait for the panther "
						+ "to move away and out of your sight. Unfortunately, the "
						+ "panther notices you, and pounces on its next dinner...\n"
						+ "You get mauled and unluckily pass away that night...\n\n"
						+ "Game Over!\n\n");
			world.setGameOver(true);
		}
	}

	@Override
	public String getHelpDescription() {
		return "[option number from list of choices]";
	}

}
