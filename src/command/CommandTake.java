package command;

import items.Container;
import items.Item;
import items.UselessItem;
import textadventure.World;

public class CommandTake extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"take", "get", "grab", "hold"};
	}

	@Override //use GET ITEMS KFJLZHNKJHTEKJHEKHJER
	public void doCommand(String cmd, String[] params, World world) {
		// Handle "take [Item]" command
		if (params.length == 1) {
			if (params.length == 1) {
				String itemName = params[0];
				if (world.getPlayer().getCurrentRoom().hasItem(itemName)) {
					Item item = world.getPlayer().getCurrentRoom().getItem(itemName);
					item.doTake(world.getPlayer().getCurrentRoom());
				} else if (world.getPlayer().hasItem(itemName)) {
					World.print("You already have that!\n\n");
				} else {
					World.print("You can't see any " + itemName + " here.\n\n");
				}
			}
			else {
				World.print("I don't understand.\n\n");
			}
		}
		// Handle "take [Item] from [Container]" command
		else if (params.length == 3) {
			// Handle errors
			if (!params[1].equals("from")) {
				World.print("I don't understand.\n\n");
				return;
			}
			if (!world.getPlayer().getCurrentRoom().hasItem(params[2])) {
				World.print("You can't see any [container] here.\n\n");
				return;
			}
			if (!(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof Container)) {
				World.print("The [container] can't hold things!\n\n");
				return;
			}
			if (!((Container)world.getPlayer().getCurrentRoom().getItem(params[2])).hasItem(params[0])) {
				World.print("The [container] doesn't have a [item]\n\n");
				return;
			// If we made it this far, then it's safe to
			// take [item] from [container]
			}
			((Container)world.getPlayer().getCurrentRoom().getItem(params[2])).doTake(((Container)world.getPlayer().getCurrentRoom().getItem(params[2])).getItem(params[0]));
		}else {
			if (params.length == 1) {
				String itemName = params[0];
				if (world.getPlayer().getCurrentRoom().hasItem(itemName)) {
					Item item = world.getPlayer().getCurrentRoom().getItem(itemName);
					item.doTake(world.getPlayer().getCurrentRoom());
				} else if (world.getPlayer().hasItem(itemName)) {
					World.print("You already have that!\n\n");
				} else {
					World.print("You can't see any " + itemName + " here.\n\n");
				}
			}
			else {
				World.print("I don't understand.\n\n");
			}
		}
	}

	@Override
	public String getHelpDescription() {
		return "[item] or [item] from [container]";
	}

}
