package command;

import items.Container;
import textadventure.World;

public class CommandPut extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[] {"put", "place"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		// Handle errors
		if (params.length != 3 && !params[1].equals("in")) {
			World.print("Invalid syntax");
			return;
		}
		if (!world.getPlayer().getCurrentRoom().hasItem(params[0]) || world.getPlayer().hasItem(params[0])){
			World.print("You can't see any [item] here!");
			return;
		}
		if (!world.getPlayer().getCurrentRoom().hasItem(params[2]) || world.getPlayer().hasItem(params[0])){
			World.print("You can't see any [container] here!");
			return;
		}
		if (!(world.getPlayer().getCurrentRoom().getItem(params[2]) instanceof Container)) {
			World.print("The [container] can't hold things.");
			return;
		}
		if (params[0].equals(params[2])) {
			World.print("You can't put the [container] into itself!");
			return;
		}
		// If we made it this far, then it's safe to put [item]
		// in [container]

		if (world.getPlayer().hasItem(params[0])) {
			((Container)world.getPlayer().getItem(params[2])).doPut(world.getPlayer().getItem(params[0]), world.getPlayer());
		}else { // current room has the item
			((Container)world.getPlayer().getItem(params[2])).doPut(world.getPlayer().getItem(params[0]), world.getPlayer().getCurrentRoom());
		}	
	}

	@Override
	public String getHelpDescription() {
		return "[item] in [container]";
	}

}
