package command;

import items.Item;
import items.ItemLockedContainer;
import items.UselessItem;
import textadventure.World;

public class CommandClimb extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[] {"climb"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if (params.length > 1) {
			World.print("Invalid syntax\n\n");
			return;
		}
		if (params.length == 0) {
			World.print("What would you like to climb?\n\n");
			return;
		}
		if (!world.getPlayer().getCurrentRoom().hasItem(params[0])){
			World.print("You can't see any " + params[0] + " here!\n\n");
			return;
		}
		if (!params[0].equals("tree") && !params[0].equals("giraffe")) {
			World.print("You can't climb the " + params[0] + ".\n\n");
			return;
		}else {
			if(params[0].equals("giraffe") && world.getPlayer().getCurrentRoom().hasItem("rare offer to get on it")) {
				if(params[0].equals("giraffe") && world.getPlayer().getCurrentRoom().hasItem("on tree")) {
					World.print("The giraffe is too far away to climb back onto now...\n\n");
				}else {
					world.getPlayer().getCurrentRoom().addItem(new UselessItem(world, "on tree", 0, Item.NOT_TAKEABLE, ""));
					World.print("You gently climb onto the giraffe's neck so as to not startle it, but it " +
								"suddenly moves on its own, walking until it reaches a tree with branches " +
								"just outside the giraffe enclosure's fences and the zoo's gates. Before " +
								"you can even blink again, the giraffe drops you onto one of the tree's " +
								"thickest branches, and walks away... Oof, what to do...\n\n");
				}
			}else if(world.getPlayer().getCurrentRoom().hasItem("rare offer to get on it")){
				World.print("Slowly, you climb down the tree, making sure to not miss a step. Though " +
							"it is late at night and you are scared by the height, you make it down " +
							"the tree and out of the zoo! You hear a car's engine sound, only to find " +
							"that your family came back for you! Very fortunately, you make your flight " +
							"and win your competition.\n\n" +
							"Congratulations! You win!\n\n");
				world.setGameOver(true);
			}else {
				World.print("Hm... the " + params[0] + " is too far away to climb...\n\n");
			}
		}
	}

	@Override
	public String getHelpDescription() {
		return "[item] (works for climbing up and down)";
	}

}
