package textadventure;

public class OutsideRoom extends Room {

	public OutsideRoom(String name, String description, boolean isLocked, World world) {
		super(name, description, isLocked, world);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void doEnter() {
		World.print("Your carpool has arrived and is waiting for you.");
		if(!getWorld().getPlayer().hasBrushedTeeth()) {
			World.print("Your carpool mates notice that you haven't brushed your teeth.  How embarrassing!  Game over!");
		}else if(getWorld().getPlayer().getHealth() != 2) {
			World.print("Your stomach churns as you realize you forgot to toast the bread.  You get sick in on the way to school.  How embarrassing!  Game over!");
		}else if(!getWorld().getPlayer().isWearingClothes()) {
			World.print("Your carpool mates notice you aren't wearing any clothes at all.  How embarrassing!  Game over!");
		}else {
			World.print("After safely getting to school, you have a wonderful rest of the day. You Win!");
			System.exit(0);
		}
	}
}
