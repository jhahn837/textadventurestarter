/*
 * Name: Julie Hahn
 * Period: 1
 * Date: 3/8/2020
 * Time taken: many hours... recorded in journal
 * Reflection: Though I found this lab overwhelming with the concept of 
 * 			   having to code a full game by myself, I found the project
 * 			   to be interesting and somewhat fun in the end because I 
 *			   was able to use my creativity. While I think the majority
 *			   of my program works without breaking, I am not fully
 *			   certain that everything as good as it can be, also because
 *			   I am not sure what the standards for a solid game are...
 *			   Still, I am quite proud that I was able to add more extensions
 *			   (ex. classes and commands) without copying a lot of the demo
 *			   program we were given.
 *
 * RESUBMISSION REFLECTION
 * 
 * Name: Julie Hahn
 * Period: 1
 * Date: 3/15/2020
 * Time taken: about 45 minutes for changes after submission
 * Reflection: While I was expecting to find many issues with my game, I
 * 			   did not find many in number, as I was expecting. Instead,
 * 			   I found that I made one major in which a last-minute 
 *			   detail (a condition in this case) prevented the player from
 *			   being able to successfully escape from the zoo and win the 
 *			   game... Though I tested this and other commands to check that
 *			   they were working and no other errors occurred, I am still
 *			   worried that I may have missed something... Still, I am glad
 *			   that I was able to catch the big mistake I made, and still
 *			   find it fascinating that I was able to complete coding such
 *			   a large project in the first place.
 * 
 */
package textadventure;

import java.util.Calendar;
import java.util.GregorianCalendar;

import interfaces.Closeable;
import items.Animal;
import items.CloseableContainer;
import items.Container;
import items.Feeder;
import items.Food;
import items.Item;
import items.ItemLockedContainer;
import items.UselessItem;

public class ZooBreakout extends World {

	public static final String ZOO_PLAZA = "THE ZOO PLAZA";
	public static final String GIRAFFE_ENCLOSURE = "GIRAFFE ENCLOSURE";
	public static final String PARROT_CAGE = "PARROT CAGE";
	public static final String DEER_PEN = "DEER PEN";
	public static final String FELINE_SAFARI = "FELINE SAFARI";

	/** Keeps track of time using built-in Java object */
	private Calendar localTime;
	long startMilliseconds;
	
	public void createPlayer() {
		setPlayer(new Player(getRoom(ZOO_PLAZA), this, "Player", "You are looking good today!"));
	}

	public void createRooms() {
		// Create rooms and add them to our HashMap
		addRoom(new Room(ZOO_PLAZA, "Surrounding you are multiple paths to various animal enclosures and exhibits. One path to the west with a sign reading �Ferocious Felines� is blocked by a bolted gate. It appears that other paths are not blocked. On the ground are various items lost or discarded by visitors.\r\n" + 
				"\n\n", this));
		addRoom(new Room(GIRAFFE_ENCLOSURE, "Before you lies a large enclosure full of tall giraffes with long necks that reach far over the enclosure�s fences. A giraffe standing next to the fence closest to you stares you down. Closely surrounding the giraffe enclosure outside the zoo�s walls is a multitude of trees. \n\n", this));
		addRoom(new Room(PARROT_CAGE, "You find yourself standing eye-to-eye with two beautiful parrots. One is a deep crimson color, and the other is a rich emerald green. While other parrots are freely squawking and chattering, the two in front of you keep their beaks shut. \n\n", this));
		addRoom(new Room(DEER_PEN, "Stepping into the petting area, you find a deer following you. Unlike the others, though, the deer keeps its jaws shut. No matter where you go, the deer follows you, yet never opens its mouth.\r\n" + 
				"\n\n", this));
		addRoom(new Room(FELINE_SAFARI, "You feel shivers down your spine, feeling as if you are being watched and hunted. \n\n", Room.LOCKED, this));
		
		// Define multiple room exits at once.  Order is north, east, south, west
		getRoom(ZOO_PLAZA).setExits(getRoom(GIRAFFE_ENCLOSURE), getRoom(DEER_PEN), getRoom(PARROT_CAGE), getRoom(FELINE_SAFARI));
		getRoom(GIRAFFE_ENCLOSURE).setExits(null, getRoom(DEER_PEN), null, getRoom(ZOO_PLAZA));
		getRoom(PARROT_CAGE).setExits(null, getRoom(ZOO_PLAZA), getRoom(DEER_PEN), null);
		getRoom(DEER_PEN).setExits(getRoom(ZOO_PLAZA), getRoom(GIRAFFE_ENCLOSURE), getRoom(PARROT_CAGE), null);

		//Plaza items
		getRoom(ZOO_PLAZA).addItem(new Food(this, "bucket_of_popcorn", 0, Item.TAKEABLE, "Old popcorn is scattered around by your feet."));
		getRoom(ZOO_PLAZA).addItem(new UselessItem(this, "dime", 10, Item.TAKEABLE, "A shiny dime lays by your feet, enticing you to take it."));
		getRoom(ZOO_PLAZA).addItem(new UselessItem(this, "magical_napkin", 10, Item.TAKEABLE, "A mysterious, yet magical-seeming napkin... It seems like you always find a coin underneath it...?"));
		
		//Giraffes items
		getRoom(GIRAFFE_ENCLOSURE).addItem(new Feeder(this, "giraffe_feeder", 15, Item.NOT_TAKEABLE, "A sign above the dispenser's knob reads '25 cents'.", Closeable.CLOSED, ItemLockedContainer.LOCKED, "quarter"));
		getRoom(GIRAFFE_ENCLOSURE).addItem(new Animal(this, "giraffe", 150, Item.NOT_TAKEABLE, "The giraffe standing next to the fence closest to you stares you down, almost looking caringly at you.", Closeable.CLOSED, ItemLockedContainer.LOCKED, "carrot"));
		getRoom(GIRAFFE_ENCLOSURE).addItem(new UselessItem(this, "tree", 300, Item.NOT_TAKEABLE, "A tall tree with many branches standing outside the giraffe enclosure and past the zoo's walls, taller than a giraffe."));
		
		//Parrots items
		getRoom(PARROT_CAGE).addItem(new Feeder(this, "bird_feeder", 15, Item.NOT_TAKEABLE, "A sign above the dispenser's knob reads '10 cents'.", Closeable.CLOSED, ItemLockedContainer.LOCKED, "dime"));
		getRoom(PARROT_CAGE).addItem(new Animal(this, "crimson_parrot", 50, Item.NOT_TAKEABLE, "A deep crimson-colored parrot stares back at you with its mouth shut but seeming to hold something.", Closeable.CLOSED, ItemLockedContainer.LOCKED, "walnut"));
		getRoom(PARROT_CAGE).addItem(new Animal(this, "emerald_parrot", 50, Item.NOT_TAKEABLE, "An emerald green-colored parrot stares at you with its break stubbornly shut closed.", Closeable.CLOSED, ItemLockedContainer.LOCKED, "walnut"));
		
		//Deer items
		getRoom(DEER_PEN).addItem(new Feeder(this, "deer_feeder", 15, Item.NOT_TAKEABLE, "A sign above the dispenser's knob reads '10 cents'.", Closeable.CLOSED, ItemLockedContainer.LOCKED, "dime"));
		getRoom(DEER_PEN).addItem(new Animal(this, "deer", 100, Item.NOT_TAKEABLE, "A deer persistently follows you, yet refuses to open its mouth.", Closeable.CLOSED, ItemLockedContainer.LOCKED, "chesnut"));
	}

	private boolean updateTime() {
		long endMilliseconds = System.currentTimeMillis();		
		localTime.add(Calendar.SECOND, (int)((endMilliseconds - startMilliseconds)/1000));
		int hour = localTime.get(Calendar.HOUR_OF_DAY);
		int minute = localTime.get(Calendar.MINUTE);
		int second = localTime.get(Calendar.SECOND);		
		startMilliseconds = endMilliseconds;
		System.out.printf("The time is %d:%02d:%02d P.M.\n\n", hour, minute, second);
		if (localTime.get(Calendar.MINUTE) == 0) { //>= 20
			print("OH NO! Now you'll be late for your flight no matter when you "
				+ "escape! You give up on escaping, but mourn the loss of your " 
				+ "once-in-a-lifetime opportunity until morning, when you are freed.\n\n");
			print("\nThank you for playing!  Better luck next time!\n\n");
			return true;
		}
		return false;
	}
	
	@Override
	public void initializeGame() {
		createRooms();
		createPlayer();
		
		// Start game timer 
		localTime = new GregorianCalendar(2020, Calendar.MARCH, 14, 11, 50, 0); // March 14, 2020 11:50 A.M.
		startMilliseconds = System.currentTimeMillis();
	}
	
	@Override
	public void printWelcome() {
		print("After coming to the zoo with your family for what was supposed to be a fun day, you "
				+ "find yourself locked inside, alone. Though you could wait until morning for someone to open the "
				+ "gates for you the next day, you are supposed to be getting on a flight to head to a "
				+ "once-in-a-lifetime competition, and getting out in the morning would mean you have to withdraw. With "
				+ "only animals and not a single unlockable gate in sight, you search for another way out of the zoo."
				+ "\nCan you make it out safely before midnight?\n\n");
		print("Welcome to Zoo Breakout!\n");
		print("(c) 2020 By Julie Hahn\n");
		print("Type 'help' if you need help with commands.\n");
		print("Type 'hints' for general hints and tips.\n");
		print("Ask for hints nicely for special hints and tips.\n\n");
		print(getPlayer().getCurrentRoom().getDescription());
		updateTime();
	}

	@Override
	public void onCommandFinished() {
		if (updateTime()) setGameOver(true);
	}

}
